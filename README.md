# Prototype for User Preferences CRUD API using Bigtable and FastAPI

## Prerequisites
1. python3
2. pycharm (IDE or use your own)

## Installing and Running the project

```
pip install -r requirements.txt
uvicorn main:app --reload
```

Swagger Documentation can be accessed at  http://127.0.0.1:8000/docs


## Architecture
This project uses FastAPI based api server to expose various endpoints with /user_preferences basepath to handle CRUD operations around user preferences
The original idea is to use hexagonal architecture to handle various aspects of the application include database interaction, business logic, deployment infrastructure and client facing APIs

As of now -> 

1. It has an API layer which exposes the endpoints
2. It has an abstract repository layer to integrate with bigtable sdk and a mock repo layer to mock bigtable operations


### Some Improvements to be done

1. Error Handling
2. Service layer to handle business operations
3. Graceful handling of user_id to avoid getting it in a duplicate way in the endpoints

