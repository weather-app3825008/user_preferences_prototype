import datetime

from google.cloud import bigtable
from google.cloud.bigtable import column_family
from google.cloud.bigtable import row_filters


# client can be cached and abstracted into another file
def create_bigtable_instance(project_id: str, instance_id: str):
    client = bigtable.Client(project=project_id, admin=True)
    return client.instance(instance_id)


class BigTableRepo:
    def __init__(self, instance, table_id):
        self.table = instance.table(table_id)

    def create_table(self, column_family_id: str):
        # Create a column family with GC policy : most recent N versions
        # Define the GC policy to retain only the most recent 2 versions
        max_versions_rule = column_family.MaxVersionsGCRule(2)
        column_families = {column_family_id: max_versions_rule}
        if not self.table.exists():
            self.table.create(column_families=column_families)

    def create_column(self, row_key: str, column_family_id: str, column: str, value: dict):
        self.create_table(column_family_id)
        row = self.table.direct_row(row_key.encode())
        row.set_cell(
            column_family_id, column, value, timestamp=datetime.datetime.utcnow()
        )

    def get_row(self, row_key: str):
        row_filter = row_filters.CellsColumnLimitFilter(1)
        return self.table.read_row(row_key.encode(), row_filter)

    def get_column(self, row_key: str, column_family_id, column):
        row = self.get_row(row_key)
        return row.cells[column_family_id][column][0]

    def delete_column(self, row_key: str, column_family_id, column):
        row = self.table.row(row_key)
        row.delete_cell(column_family_id=column_family_id, column=column)
        row.commit()
