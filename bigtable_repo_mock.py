import datetime

from google.cloud import bigtable
from google.cloud.bigtable import column_family
from google.cloud.bigtable import row_filters


class BigTableRepoMock:
    def __init__(self, instance, table_id):
        self.table = {}

    def create_column(self, row_key: str, column_family_id: str, column: str, value: dict):
        self.table[row_key] = self.table.get(row_key, {})
        self.table[row_key][column_family_id] = self.table.get(row_key, {}).get(column_family_id, {})
        self.table[row_key][column_family_id][column] = value

    def get_row(self, row_key: str):
        return self.table.get(row_key, {})

    def get_column(self, row_key: str, column_family_id, column):
        row = self.get_row(row_key)
        return row.get(column_family_id, {})[column]

    def delete_column(self, row_key: str, column_family_id, column):
        self.table[row_key][column_family_id][column].pop(column)
