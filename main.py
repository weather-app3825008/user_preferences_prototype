from fastapi import FastAPI

from bigtable_repo import BigTableRepo, create_bigtable_instance
from setup_user_preferences import setup_user_preferences

app = FastAPI()


# bigtable_client = create_bigtable_instance(project_id='', instance_id='')


@app.get("/")
async def root():
    return {"message": "API is working!"}


setup_user_preferences(app=app, bigtable_client=None)
