from fastapi import FastAPI

from bigtable_repo import BigTableRepo
from bigtable_repo_mock import BigTableRepoMock
from user_preferences import UserPreference


def setup_user_preferences(app: FastAPI, bigtable_client):
    user_preferences_repo = BigTableRepoMock(bigtable_client, 'user_preferences')

    @app.get("/user_preferences/{user_id}")
    async def find(user_id: str) -> dict:
        user_preferences = user_preferences_repo.get_row(row_key=user_id)
        return user_preferences

    @app.get("/user_preferences/{user_id}/{type}/{key}")
    async def get(user_id: str, type: str, key: str) -> dict:
        return user_preferences_repo.get_column(row_key=user_id, column_family_id=type, column=key)

    @app.post("/user_preferences/{user_id}")
    async def save(preference: UserPreference) -> UserPreference:
        user_preferences_repo.create_column(row_key=preference.user_id, column_family_id=preference.type,
                                            column=preference.name, value=preference.value)
        # user_id could be fetched from fast api session or from jwts and we might need to set it again on preference
        return preference

    @app.delete("/user_preferences/{user_id}/{type}/{key}")
    async def delete(user_id: str, type: str, key: str):
        user_preferences_repo.delete_column(row_key=user_id, column_family_id=type, column=key)
