from dataclasses import dataclass
from typing import Optional


@dataclass
class UserPreference:
    type: str
    name: str
    value: dict
    user_id: str
    key: Optional[str]
